﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LooseCollider : MonoBehaviour
{
    [SerializeField] string escena;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine( loadscene());
    }

   IEnumerator loadscene()
    {
        yield return new WaitForSeconds(0.2f);
        SceneManager.LoadScene(escena);
    }
}
