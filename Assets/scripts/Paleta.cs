﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paleta : MonoBehaviour
{
    //config
    [SerializeField] float minX = 1.2f;
    [SerializeField] float maxX = 14.7f;
    [SerializeField] float width = 16f;

    //cached references
    GameSession theGameSession;
    Ball theBall;



    // Start is called before the first frame update
    void Start()
    {
        theGameSession = FindObjectOfType<GameSession>();
        theBall = FindObjectOfType<Ball>();
    }

    // Update is called once per frame
    void Update() 
    {
        float mousepos = Input.mousePosition.x/Screen.width * width;
        Vector2 PaddlePos = new Vector2(transform.position.x, transform.position.y);
        PaddlePos.x = Mathf.Clamp(GetPosX(), minX, maxX);
        transform.position = PaddlePos;
    }

    private float GetPosX()
    {
        if (theGameSession.IsAutoPlayEnabled())
        {
            return theBall.transform.position.x;
        }
        else
        {
            return Input.mousePosition.x / Screen.width * width;
        }
    }
}
