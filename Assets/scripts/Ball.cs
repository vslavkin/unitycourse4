﻿using System.Collections;
using UnityEngine;

public class Ball : MonoBehaviour
{
    //config
    [SerializeField] Paleta paleta1;
    [SerializeField] float xPush = 2f;
    [SerializeField] float yPush = 15f;
    [SerializeField] AudioClip[] Sonidos;
    [SerializeField] float randomFactor = 0.2f;


    //estado
    Vector2 ballpaddleVector;
    bool SeApreto = false;

    //Cached component references
    AudioSource myAudioSource;
    Rigidbody2D myRigidBody2D;
    GameSession theGameSession;

    // Start is called before the first frame update
    void Start()
    {
        theGameSession = FindObjectOfType<GameSession>();
        ballpaddleVector = transform.position - paleta1.transform.position;
        myAudioSource = GetComponent<AudioSource>();
        myRigidBody2D = GetComponent<Rigidbody2D>();
        StartCoroutine(Autothrow());

    }

    IEnumerator Autothrow()
    {
        if (theGameSession.IsAutoPlayEnabled())
        {
            yield return new WaitForSeconds(1);
            Throwball();
            SeApreto = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!SeApreto)
        {
            ballpaddle();
            mousebutton();
        }

    }

    private void mousebutton()
    {
        if (Input.GetMouseButtonDown(0))
        {
            SeApreto = true;
            Throwball();
        }
    }

    public void Throwball()
    {
        myRigidBody2D.velocity = new Vector2(Random.Range(xPush,(xPush * -1)), yPush);

    }

    private void ballpaddle()
    {
        Vector2 paletaPos = new Vector2(paleta1.transform.position.x, paleta1.transform.position.y);
        transform.position = paletaPos + ballpaddleVector;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 velocityTweak = new Vector2
            (Random.Range(0, randomFactor),
            Random.Range(0f, randomFactor));

        if (SeApreto)
        {
            AudioClip clip = Sonidos[UnityEngine.Random.Range(0, Sonidos.Length)];
            myAudioSource.PlayOneShot(clip);
            myRigidBody2D.velocity += velocityTweak;
        }
        
    }
}
