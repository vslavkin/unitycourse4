﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    // config params
    [SerializeField] AudioClip[] Sonidos;
    [SerializeField] GameObject particulas;
    [SerializeField] Sprite[] hitSprites;

    //cached reference
    Level level;

    //state variables
    [SerializeField] int timesHit;
 

    private void Start()
    {
        CountBreakableBlocks();

    }

    private void CountBreakableBlocks()
    {
        level = FindObjectOfType<Level>();
        if (tag == "Breakable")
        {
            level.blockcounter();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (tag == "Breakable")
        {
            HandleHit();

        }
   

    }

    

    private void HandleHit()
    {
        timesHit++;
        int HP = hitSprites.Length + 1;
        if (timesHit >= HP)
        {
            DestroyBlock();
        }
        else
        {
     
             ShowNextSprite();
            
        }
    }

    private void ShowNextSprite()
    {
        int spriteIndex = timesHit - 1;

        if (hitSprites[spriteIndex] != null)
        {
            GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
        else
        {
            Debug.LogError("Block sprite is empty" + gameObject.name);
        }
    }

    private void DestroyBlock()
    {
        PlayClip();
        Destroy(gameObject);
        level.blockdestroyed();
        TriggerParticles();
    }

    private void PlayClip()
    {
        FindObjectOfType<GameSession>().AddToScore();
        AudioClip clip = Sonidos[UnityEngine.Random.Range(0, Sonidos.Length)];
        AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
    }


    private void TriggerParticles()
    {
        GameObject sparkles = Instantiate(particulas, transform.position, transform.rotation);
        Destroy(sparkles, 1f);
    }
}
