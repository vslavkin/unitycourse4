﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    [SerializeField] int Blocks;
    // Start is called before the first frame update
    [SerializeField] string NextScene;

    scenes scenes ;


    private void Start()
    {
        scenes = FindObjectOfType<scenes>();
        
    }
    public void blockcounter()
    {
        Blocks++;
    }

    public void blockdestroyed()
    {
        Blocks--;
        if (Blocks < 1)
        {
            scenes.Escenas(NextScene);
        }
    }
    

}
