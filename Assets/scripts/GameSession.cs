﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameSession : MonoBehaviour
{
    [Range(0.1f, 10f)] [SerializeField] float gameSpeed=1f;
    float gameSpeedBackup;
    [SerializeField] int PointsPerBlock = 57;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] int currentScore = 0;
    [SerializeField] bool isAutoPlayEnabled;
    [SerializeField] GameObject gamezonedivision;
    [SerializeField]float timeLeft = 5f;
    float timebackup;
    [SerializeField] GameObject ball;

    //cached refereces
    GameSession[] gameSessions;


  


    private void Awake()
    {
        gameSessions = FindObjectsOfType<GameSession>();
        int gameStatusCount = gameSessions.Length;
        if (gameStatusCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        scoreText.text = "score : " + currentScore.ToString();

        timebackup = timeLeft;
        gameSpeedBackup = gameSpeed;

    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = gameSpeed;
        Timer();
    }

    private void Timer()
    {
        if (ball.transform.position.y > gamezonedivision.transform.position.y)
        {
            timeLeft -= Time.deltaTime;
        }

        else
        {
            timeLeft = timebackup;
            gameSpeed = gameSpeedBackup;
        }
            
        
        if (timeLeft < 0)
        {
            gameSpeed = gameSpeedBackup + 0.5f;
        }

    }

    public void AutoPlay()
    {
        isAutoPlayEnabled = true;
    }

    public void AddToScore()
    {
        currentScore += PointsPerBlock;
        scoreText.text = "score : " + currentScore.ToString();
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }

    public bool IsAutoPlayEnabled()
    {
        return isAutoPlayEnabled;
    }

}
