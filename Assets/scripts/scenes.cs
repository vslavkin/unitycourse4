﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;




public class scenes : MonoBehaviour
{


    public void Escenas(string escena)
    {
        SceneManager.LoadScene(escena);
    }

    public void LoadScene0()
    {
        SceneManager.LoadScene(0);
        FindObjectOfType<GameSession>().ResetGame();
    }


    public void LoadNextScene()
    {
        int currentscene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentscene + 1);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
