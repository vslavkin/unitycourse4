﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class prueba: MonoBehaviour
{
    [SerializeField]
    private Object m_SceneAsset;
   /* [SerializeField]
    private string m_SceneName = "";
    public string SceneName
    {
        get { return m_SceneName; }
    }
   */
  
    public string nombre
    {
        get { return m_SceneAsset.name; }
    }

    // makes it work with the existing Unity methods (LoadLevel/LoadScene)
    /*public static implicit operator string (prueba sceneField)
    {
        return sceneField.SceneName;
    }
    */
    
    public void escena()
    {
        SceneManager.LoadScene(nombre);
    }

    public void quitar()
    {
        Application.Quit();
    }
}



