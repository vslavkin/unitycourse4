﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class flechitas : MonoBehaviour
{
    [SerializeField] Button []botones;



    private void Update()
    {
        var pointer = new PointerEventData(EventSystem.current);

        for (int cantidad = 0; cantidad < botones.Length; cantidad++) ;

        // Start is called before the first frame update
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ExecuteEvents.Execute(botones[0].gameObject, pointer, ExecuteEvents.pointerEnterHandler);
        }
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            ExecuteEvents.Execute(botones[0].gameObject, pointer, ExecuteEvents.submitHandler);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            ExecuteEvents.Execute(botones[1].gameObject, pointer, ExecuteEvents.pointerEnterHandler);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            ExecuteEvents.Execute(botones[1].gameObject, pointer, ExecuteEvents.submitHandler);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            ExecuteEvents.Execute(botones[2].gameObject, pointer, ExecuteEvents.pointerEnterHandler);
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            ExecuteEvents.Execute(botones[2].gameObject, pointer, ExecuteEvents.submitHandler);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            ExecuteEvents.Execute(botones[3].gameObject, pointer, ExecuteEvents.pointerEnterHandler);
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            ExecuteEvents.Execute(botones[3].gameObject, pointer, ExecuteEvents.submitHandler);
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            ExecuteEvents.Execute(botones[4].gameObject, pointer, ExecuteEvents.pointerEnterHandler);
        }
        if (Input.GetKeyUp(KeyCode.Return))
        {
            ExecuteEvents.Execute(botones[4].gameObject, pointer, ExecuteEvents.submitHandler);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExecuteEvents.Execute(botones[5].gameObject, pointer, ExecuteEvents.pointerEnterHandler);
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            ExecuteEvents.Execute(botones[5].gameObject, pointer, ExecuteEvents.submitHandler);
        }

    }
    }
